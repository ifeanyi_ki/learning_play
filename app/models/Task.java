package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

/**
 * Created by KINGLEE on 11/10/16.
 */
@Entity
public class Task extends Model{

    @Id
    public Long id;

    @Constraints.Required
    public String label;

    public static Finder<Long, Task> find = new Finder(Long.class, Task.class);

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public static List<Task> all(){
        return find.all();
    }

    public static void create(Task task){
        task.save();
    }

    public static void delete(Long id){
        find.ref(id).delete();
    }
}
